(function () {
    class Updater extends OpenBlock.VersionUpdater {
        constructor() {
            super(3);
        }
        async update() {
            let p = new Promise((resolve, reject) => {
                OpenBlock.VFS.partition.config.get('project.json', p => {
                    if (p) {
                        if ((!p.version) || p.version < 2) {
                            console.warn('项目版本过低，无法升级');
                            reject();
                            return;
                        }
                        if (p.version >= 3) {
                            resolve();
                            return;
                        }
                    }
                    // p.version = 3;
                    // OpenBlock.VFS.partition.config.put('project.json', p);
                    console.info('升级程序版本到 v3');
                    resolve();
                });
                resolve();
            });
            return p;
        }
    }
    OpenBlock.Version.addUpdater(new Updater());
})();