/**
 * @license
 * Copyright 2021 Du Tian Wei
 * SPDX-License-Identifier: Apache-2.0
 */
import Template from '../jsruntime/developtools/template.mjs';
import IframeSimulator from '../iframeconnector/IframeSimulator.mjs';
import ToPython from '../jsruntime/developtools/python/ToPython.mjs'
import ToC from '../jsruntime/developtools/c/ToC.mjs';
import OpenBrotherSerialProtocol from './openBrotherSerialProtocol.mjs';
import SceneManager from '../frontpage/core/ub/SceneManager.mjs';
import '../frontpage/js/htmls/SceneManager/SceneManager.mjs';
export default async function () {
    await OpenBlock.Utils.loadJSAsync([
        // '../../js/htmls/SceneManager/SceneManager.mjs',
        "../jsruntime/test/env/native-web3d.js",
        "../jsruntime/test/env/i18n_zh.js",
        "../jsruntime/test/env/native.js"]);
    let openBrotherTemplate = await axios.get('../static/openBrother.template.py');
    openBrotherTemplate = Template.parseTemplate(openBrotherTemplate.data, '#');
    let simulator = new IframeSimulator();
    let simulator3D = new IframeSimulator('../jsruntime/web3d/index.html');
    let scenes = [
        {
            name: 'web',
            env: 'web',
            beforeCreate(scene) {
                console.log('beforeCreate', scene);
                return scene;
            },
            beforeUpdate(scene) {
                console.log('beforeUpdate', scene);
                return scene;
            },
            runMethods: [
                {
                    icon: 'ios-play',
                    name: OB_IDE.$t('模拟器运行'),
                    async run(scene) {
                        await OB_IDE.saveAllTabs();
                        OpenBlock.exportExePackage(scene, (e, buf) => {
                            if (e) {
                                OB_IDE.$Message.error(OB_IDE.$t('编译失败'));
                            } else {
                                simulator.runProject(buf, scene);
                            }
                        });
                    }
                },
                {
                    icon: 'ios-apps',
                    name: OB_IDE.$t('新模拟器运行'),
                    async run(scene) {
                        await OB_IDE.saveAllTabs();
                        OpenBlock.exportExePackage(scene, (e, buf) => {
                            if (e) {
                                OB_IDE.$Message.error(OB_IDE.$t('编译失败'));
                            } else {
                                simulator.newWindow(buf, scene);
                            }
                        });
                    }
                },
                {
                    icon: 'ios-browsers',
                    name: OB_IDE.$t('独立窗口运行'),
                    async run(scene) {
                        await OB_IDE.saveAllTabs();
                        OpenBlock.exportExePackage(scene, (e, buf) => {
                            if (e) {
                                OB_IDE.$Message.error(OB_IDE.$t('编译失败'));
                            } else {
                                simulator.standaloneWindow(buf, scene);
                            }
                        });
                    }
                },
                {
                    icon: 'ios-code-download',
                    name: OB_IDE.$t('导出字节码'),
                    async run(scene) {
                        await OB_IDE.saveAllTabs();
                        OpenBlock.exportExePackage(scene, (e, buf) => {
                            if (e) {
                                OB_IDE.$Message.error(OB_IDE.$t('编译失败'));
                            } else {
                                FileOD.Save(scene.name + '.web.xe', new Blob([buf]));
                            }
                        });
                    }
                },
            ]
        },
        {
            name: "editorExt",
            env: "editorExt",
            runMethods: [
                {
                    icon: 'ios-code-download',
                    name: OB_IDE.$t('导出字节码'),
                    run(scene) {
                        OpenBlock.saveAllSrc();
                        OpenBlock.exportExePackage(scene, (e, buf) => {
                            if (e) {
                                OB_IDE.$Message.error(OB_IDE.$t('编译失败'));
                            }
                            FileOD.Save(scene.name + '.editorExt.xe', new Blob([buf]));
                        });
                    }
                },]
        },
        {
            name: "openBrother-C",
            env: "openBrother-C",
            runMethods: [
                {
                    icon: "md-game-controller-b",
                    name: OB_IDE.$t('下载C代码'),
                    async run(scene) {
                        let c = await ToC.buildLogicSrc({
                            env: scene.env,
                            srcList: scene.srcList,
                            entry: scene.entry,
                            envTemplate: openBrotherTemplate
                        });
                        console.log(c);
                        FileOD.Save(scene.name + '.openBrother.c', c);
                    }
                },
            ]
        },
        {
            name: 'web3d',
            env: 'web3d',
            beforeCreate(scene) {
                console.log('beforeCreate', scene);
                return scene;
            },
            beforeUpdate(scene) {
                console.log('beforeUpdate', scene);
                return scene;
            },
            runMethods: [
                {
                    icon: 'ios-play',
                    name: OB_IDE.$t('模拟器运行'),
                    async run(scene) {
                        await OB_IDE.saveAllTabs();
                        OpenBlock.exportExePackage(scene, (e, buf) => {
                            if (e) {
                                OB_IDE.$Message.error(OB_IDE.$t('编译失败'));
                            } else {
                                simulator3D.runProject(buf, scene);
                            }
                        });
                    }
                },
                {
                    icon: 'ios-apps',
                    name: OB_IDE.$t('新模拟器运行'),
                    async run(scene) {
                        await OB_IDE.saveAllTabs();
                        OpenBlock.exportExePackage(scene, (e, buf) => {
                            if (e) {
                                OB_IDE.$Message.error(OB_IDE.$t('编译失败'));
                            } else {
                                simulator3D.newWindow(buf, scene);
                            }
                        });
                    }
                },
                {
                    icon: 'ios-browsers',
                    name: OB_IDE.$t('独立窗口运行'),
                    async run(scene) {
                        await OB_IDE.saveAllTabs();
                        OpenBlock.exportExePackage(scene, (e, buf) => {
                            if (e) {
                                OB_IDE.$Message.error(OB_IDE.$t('编译失败'));
                            } else {
                                simulator3D.standaloneWindow(buf, scene);
                            }
                        });
                    }
                },
                {
                    icon: 'ios-code-download',
                    name: OB_IDE.$t('导出字节码'),
                    async run(scene) {
                        await OB_IDE.saveAllTabs();
                        OpenBlock.exportExePackage(scene, (e, buf) => {
                            if (e) {
                                OB_IDE.$Message.error(OB_IDE.$t('编译失败'));
                            } else {
                                FileOD.Save(scene.name + '.web.xe', new Blob([buf]));
                            }
                        });
                    }
                },
            ]
        },
    ];
    for (let i in scenes) {
        let s = scenes[i];
        if (!OpenBlock.Env.hasEnv(s.env)) {
            OpenBlock.Env.addNewEnv(s.env);
        }
        SceneManager.addAvailableScene(s);
    }

    OpenBlock.Utils.handleUrlHash(() => {
        OB_IDE.$Message.info(OB_IDE.$t('正在打开工程，请稍后'));
        simulator.loading = true;
        showOverallLoading();
    }, async (error, loaded) => {
        if (!loaded) {
            await new Promise((resolve, reject) => {
                axios.get('/openblock/defaultproject.obp', {
                    responseType: 'arraybuffer', // default
                }).then(({ data }) => {
                    OpenBlock.importProjectZip(data, resolve);
                }).catch((e) => {
                    console.log(e);
                    resolve();
                });
            });
        }
        await OpenBlock.Version.updateVersion();
        await OpenBlock.compileAllSrc();
        simulator.loading = false;
        hideOverallLoading();
        OB_IDE.$Message.success(OB_IDE.$t('工程加载完成'));
    });
}